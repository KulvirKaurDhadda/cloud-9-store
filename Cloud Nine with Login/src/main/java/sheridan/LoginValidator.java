package sheridan;

public class LoginValidator {
	
	/*
	 * @author Kulvir Kaur Dhadda - 991539865 */
	
	public static final String REGEX = "^[a-zA-Z][a-zA-Z0-9]{5,}$";

	public static boolean isValidLoginName( String loginName ) {
		return loginName.matches(REGEX);
	}
}

package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Kulvir Kaur Dhadda - 991539865 
 * */
public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		String login = "kulvirDhadda";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertTrue("The login entered is valid", isValid);
	}
	@Test
	public void testIsValidLoginException( ) {
		String login = "";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertFalse("The login entered is not valid", isValid);
	}
	@Test
	public void testIsValidLoginBoundryIn( ) {
		String login = "kulvir";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertTrue("The login entered is valid", isValid);
	}
	@Test
	public void testIsValidLoginBoundryOut( ) {
		String login = "kulvi";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertFalse("The login entered is not valid", isValid);
	}
	@Test
	public void testIsValidLoginLetterRegular( ) {
		String login = "KulvirDhadda";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertTrue("The login entered is valid", isValid);
	}
	@Test
	public void testIsValidLoginLetterException( ) {
		String login = " ulvirDhadda";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertFalse("The login entered is not valid", isValid);
	}
	@Test
	public void testIsValidLoginLetterBoundryIn( ) {
		String login = "kulvirDhadda";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertTrue("The login entered is valid", isValid);
	}
	@Test
	public void testIsValidLoginLetterBoundryOut( ) {
		String login = "9ulvirDhadda";
		boolean isValid = LoginValidator.isValidLoginName(login);
		assertFalse("The login entered is not valid", isValid);
	}

}
